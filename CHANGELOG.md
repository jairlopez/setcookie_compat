# Change Log

## 2.0.0

- *Date:* April 8th, 2020

* Drop compatibility with PHP < 5.6
* Throw exceptions like PHP 8.0.3's `setcookie`
* Use PHP's built-in testing mechanism

## 1.0

- *Date:* July 20th, 2020

Initial release
