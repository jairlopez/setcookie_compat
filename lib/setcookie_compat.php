<?php
/**
	setcookie_compat: A Compatibility library with PHP 7.4's setcookie API
	                  for projects requiring PHP >= 5.3.

	MIT License

	Copyright (c) 2020 Jair López <jair_lopez4321@hotmail.com> and contributors

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.
 */
namespace { // global scope
	if (!class_exists('\Error')) {
		class Error extends \Exception {
		}
	}

	if (!class_exists('\TypeError')) {
		class TypeError extends \Error {
		}
	}

	if (!class_exists('\ArgumentCountError')) {
		class ArgumentCountError extends \TypeError {
		}
	}


	if (!class_exists('\ValueError')) {
		class ValueError extends \Error {
		}
	}

	if (!function_exists('setrawcookie_compat')) {
		/**
		* Drop-in replacement for standard setrawcookie() allowing
		* older versions of PHP to send cookies using the PHP 7.4's setrawcookie API
		*
		* @param string $name     Cookie's name
		* @param string $value    Cookie's value. It will not be rawurlencouded.
		* @param mixed  $options  It can be one of these:
		*                         (1) An expiration date (a timestamp)
		*                         (2) An associative array with
		*                         extra information about the cookie
		*                         being sent. Valid keys are: 'expires'
		*                         (a timestamp), 'path' (string),
		*                         'domain' (string), 'secure' (bool),
		*                         'httponly' (bool), and 'samesite'
		*                         (string).
		* @param string $path     Only available if $options is an
		*                         expiration date. Cookie's path
		* @param string $domain   Only available if $options is an
		*                         expiration date. Cookie's domain
		* @param bool   $secure   Only available if $options is an
		*                         expiration date. Whether it should be
		*                         sent over secure connections
		* @param bool   $httponly Only available if $options is an
		*                         expiration date. Whether it should be
		*                         unavailable for clients
		* @param string $samesite Only available if $options is an
		*                         expiration date. Cookie context; it
		*                         can be 'Strict', 'Lax', or 'None'
		*
		* @return true|false|null true if the cookie was sent
		*                         successfully, false if it was not
		*                         sent; it returns null on some error
		*                         conditions
		*/
		function setrawcookie_compat() {
			$result = null;
			$php_ver = phpversion();
			$args = func_get_args();
			$error_strings = array(
				'args_after_array' => 'setrawcookie(): Expects exactly 3 arguments when argument #3 ($expires_or_options) is an array',
				'empty' => 'setrawcookie(): Argument #%d ($%s) cannot be empty',
				'empty_args' => 'setrawcookie() expects at least 1 argument, 0 given',
				'expires_greater_than_9999' => 'setrawcookie(): "expires" option cannot have a year greater than 9999',
				'invalid_arg_type' => 'setrawcookie(): Argument #%d ($%s) must be of type %s, %s given',
				'invalid_chars_in_arg' => 'setrawcookie(): Argument #%d ($%s) cannot contain %s',
				'invalid_chars_in_opt' => 'setrawcookie(): "%s" option cannot contain %s',
				'invalid_option' => 'setrawcookie(): option "%s" is invalid',
				'numeric_keys' => 'setrawcookie(): option array cannot have numeric keys',
				'obj_not_string' => 'Object of class %s could not be converted to string',
				'too_many_args' => 'setrawcookie() expects at most 7 arguments, %d given'
			);

			if (version_compare($php_ver, '8.0') >= 0) {
				$result = call_user_func_array('setrawcookie', $args);
			} else {
				$reserved_chars_list_opt = ",; \t\r\n\v\f";
				$reserved_chars_list = "=" . $reserved_chars_list_opt;
				$n_args = count($args);

				if ($n_args) {
					if ($n_args > 7) {
						throw new \ArgumentCountError(sprintf($error_strings['too_many_args'], $n_args));
					}

					if (
						// PHP 5.6 returns true even when cookie names are empty.
						// PHP 5.5 doesn't return 'name=deleted' when deleting cookies.
						(empty($args[0]) || empty($args[1])) && !(isset($args[2]) && is_array($args[2]))

						// PHP 5.4 doesn't add 'Max-Age' when 'expires' is specified.
						|| version_compare($php_ver, '5.5.0') < 0 && (isset($args[2]) && !is_array($args[2]))
					) {
						// It uses the old signature

						// Use new signature instead.
						$new_args = array();
						$options = array();

						if (isset($args[0])) {
							$new_args[] = $args[0];
						} else {
							$new_args[] = null;
						}

						if (isset($args[1])) {
							$new_args[] = $args[1];
						} else {
							$new_args[] = null;
						}

						if (isset($args[2])) {
							$options['expires'] = $args[2];
						}

						if (isset($args[3])) {
							$options['path'] = $args[3];
						}

						if (isset($args[4])) {
							$options['domain'] = $args[4];
						}

						if (isset($args[5])) {
							$options['secure'] = $args[5];
						}

						if (isset($args[6])) {
							$options['httponly'] = $args[6];
						}

						$new_args[] = $options;

						$args = $new_args;
					}
				} else {
					// PHP 5.6 returns false when called with no arguments.
					throw new \ArgumentCountError($error_strings['empty_args']);
				}

				// Validate `$name` argument.
				if (empty($args[0])) {
					throw new \ValueError(sprintf($error_strings['empty'], 1, 'name'));
				} elseif (is_array($args[0])) {
					throw new \TypeError(sprintf($error_strings['invalid_arg_type'], 1, 'name', 'string', 'array'));
				} elseif (is_object($args[0])) {
					throw new \TypeError(sprintf($error_strings['invalid_arg_type'], 1, 'name', 'string', get_class($args[0])));
				} elseif (strpbrk($args[0], $reserved_chars_list) !== false) {
					throw new \ValueError(sprintf($error_strings['invalid_chars_in_arg'], 1, 'name', '"=", ",", ";", " ", "\t", "\r", "\n", "\013", or "\014"'));
				}

				// Validate `$value` argument.
				if (isset($args[1])) {
					if (is_array($args[1])) {
						throw new \TypeError(sprintf($error_strings['invalid_arg_type'], 2, 'value', 'string', 'array'));
					} elseif (is_object($args[1])) {
						throw new \TypeError(sprintf($error_strings['invalid_arg_type'], 2, 'value', 'string', get_class($args[1])));
					} elseif (strpbrk($args[1], $reserved_chars_list_opt) !== false) {
						throw new \ValueError(sprintf($error_strings['invalid_chars_in_arg'], 2, 'value', '",", ";", " ", "\t", "\r", "\n", "\013", or "\014"'));
					}
				}

				// Get cookie options
				if (isset($args[2])) {
					if (is_string($args[2])) {
						throw new \TypeError(sprintf($error_strings['invalid_arg_type'], 3, 'expires_or_options', 'array|int', 'string'));
					} elseif (is_object($args[2])) {
						throw new \TypeError(sprintf($error_strings['invalid_arg_type'], 3, 'expires_or_options', 'array|int', get_class($args[2])));
					} else {
						$use_old_signature = !is_array($args[2]);

						// PHP 5.6 may output negative values for Max-Age.
						if (!is_array($args[2]) && $args[2] < time()) {
							// Use new signature
							$use_old_signature = false;
							$options = array();

							$options['expires'] = $args[2];

							if (isset($args[3])) {
								$options['path'] = $args[3];
							}

							if (isset($args[4])) {
								$options['domain'] = $args[4];
							}

							if (isset($args[5])) {
								$options['secure'] = $args[5];
							}

							if (isset($args[6])) {
								$options['httponly'] = $args[6];
							}

							$args = array($args[0], $args[1], $options);
						}

						if ($use_old_signature) {
							// A legacy setcookie call.

							// Validates `$expires` argument.
							if (isset($args[2])) {
								$expires = (int) $args[2];

								// Make sure there is no overflow.
								if ($expires == $args[2] && $expires > 0 && strlen(date('Y', $expires)) > 4) {
									throw new \ValueError($error_strings['expires_greater_than_9999']);
								}
							}

							// Validate `path` argument.
							if (isset($args[3])) {
								if (is_array($args[3])) {
									throw new \TypeError(sprintf($error_strings['invalid_arg_type'], 4, 'path', 'string', 'array'));
								} elseif (is_object($args[3])) {
									throw new \TypeError(sprintf($error_strings['invalid_arg_type'], 4, 'path', 'string', get_class($args[3])));
								} elseif (strpbrk($args[3], $reserved_chars_list_opt) !== false) {
									throw new \ValueError(sprintf($error_strings['invalid_chars_in_arg'], 4, 'path', '",", ";", " ", "\t", "\r", "\n", "\013", or "\014"'));
								}
							}

							// Validate `domain` argument.
							if (isset($args[4])) {
								if (is_array($args[4])) {
									throw new \TypeError(sprintf($error_strings['invalid_arg_type'], 5, 'domain', 'string', 'array'));
								} elseif (is_object($args[4])) {
									throw new \TypeError(sprintf($error_strings['invalid_arg_type'], 5, 'domain', 'string', get_class($args[4])));
								} elseif (strpbrk($args[4], $reserved_chars_list_opt) !== false) {
									throw new \ValueError(sprintf($error_strings['invalid_chars_in_arg'], 5, 'domain', '",", ";", " ", "\t", "\r", "\n", "\013", or "\014"'));
								}
							}

							// Validate `secure` argument.
							if (isset($args[5])) {
								if (is_array($args[5])) {
									throw new \TypeError(sprintf($error_strings['invalid_arg_type'], 6, 'secure', 'bool', 'array'));
								} elseif (is_object($args[5])) {
									throw new \TypeError(sprintf($error_strings['invalid_arg_type'], 6, 'secure', 'bool', get_class($args[5])));
								}
							}

							// Validate `httponly` argument.
							if (isset($args[6])) {
								if (is_array($args[6])) {
									throw new \TypeError(sprintf($error_strings['invalid_arg_type'], 7, 'httponly', 'bool', 'array'));
								} elseif (is_object($args[6])) {
									throw new \TypeError(sprintf($error_strings['invalid_arg_type'], 7, 'httponly', 'bool', get_class($args[6])));
								}
							}

							// PHP 7.4 returns false (instead of true) when any parameter contains NUL.
							foreach ($args as $arg) {
								if (is_string($arg) && strpbrk($arg, "\0") !== false) {
									$invalid = trigger_error('Header may not contain NUL bytes', E_USER_WARNING);
								}
							}

							if (!isset($invalid)) {
								// It may return null.
								$result = call_user_func_array('setrawcookie', $args);
							} else {
								$result = false;
							}
						} else {
							// A setcookie call with the new signature.

							if (isset($args[3])) {
								throw new \ArgumentCountError($error_strings['args_after_array']);
							} else {
								// Get cookie options.
								$options = $args[2];
								$keys = array_keys($options);

								foreach ($keys as $index) {
									if (!is_numeric($index)) {
										if (!in_array($index, array('expires', 'path', 'domain', 'secure', 'httponly', 'samesite'))) {
											throw new \ValueError(sprintf($error_strings['invalid_option'], $index));
										}
									} else {
										throw new \ValueError($error_strings['numeric_keys']);
									}
								}
							}

							$raw_header = 'Set-Cookie: ';
							$raw_header .= $args[0] . '=';

							if ((string) $args[1] === '') {
								// Delete cookie.
								$raw_header .= 'deleted; expires=' . gmdate('D, d-M-Y H:i:s T', 1) . '; Max-Age=0';
							} else {
								if (isset($args[1])) {
									$raw_header .= $args[1];
								}

								// Add 'expires' and 'Max-Age' attributes.
								if (isset($options['expires'])) {
									$is_expires_int = is_numeric($options['expires']);

									// It will generate a warning message unless it is an integer.
									$expires = sprintf("%d", $options['expires']);

									if (!$is_expires_int && $expires != $options['expires'] && !is_string($options['expires'])) {
										// It was not an integer
										$expires = 1;
										$options['expires'] = 1;
									}

									// Make sure there's no overflow.
									if ($expires == $options['expires'] && $expires > 0) {
										if (strlen(date('Y', $expires)) < 5) {
											$maxage = $expires - time();

											if ($maxage < 0) {
												$maxage = 0;
											}

											$raw_header .= '; expires=' . gmdate('D, d-M-Y H:i:s T', $expires) . '; Max-Age=' . $maxage;
										} else {
											throw new \ValueError($error_strings['expires_greater_than_9999']);
										}
									}
								}

								// Add 'path' attribute.
								if (!empty($options['path'])) {
									// Validate path
									if (is_object($options['path']) && !method_exists($options['path'], '__toString')) {
										throw new \Error(sprintf($error_strings['obj_not_string'], get_class($options['path'])));
									}

									$options['path'] = (string) $options['path'];

									if (strpbrk($options['path'], $reserved_chars_list_opt) === false) {
										$raw_header .= '; path=' . $options['path'];
									} else {
										throw new \ValueError(sprintf($error_strings['invalid_chars_in_opt'], 'path', '",", ";", " ", "\t", "\r", "\n", "\013", or "\014"'));
									}
								}

								// Add 'domain' attribute.
								if (!empty($options['domain'])) {
									// Validate domain
									if (is_object($options['domain']) && !method_exists($options['domain'], '__toString')) {
										throw new \Error(sprintf($error_strings['obj_not_string'], get_class($options['domain'])));
									}

									$options['domain'] = (string) $options['domain'];

									if (strpbrk($options['domain'], $reserved_chars_list_opt) === false) {
										$raw_header .= '; domain=' . $options['domain'];
									} else {
										throw new \ValueError(sprintf($error_strings['invalid_chars_in_opt'], 'domain', '",", ";", " ", "\t", "\r", "\n", "\013", or "\014"'));
									}
								}

								// Add 'secure' attribute.
								if (!empty($options['secure'])) {
									$raw_header .= '; secure';
								}

								// Add 'HttpOnly' attribute.
								if (!empty($options['httponly'])) {
									$raw_header .= '; HttpOnly';
								}

								// Add 'SameSite' attribute.
								if (!empty($options['samesite'])) {
									if (is_object($options['samesite']) && !method_exists($options['samesite'], '__toString')) {
										throw new \Error(sprintf($error_strings['obj_not_string'], get_class($options['samesite'])));
									}

									$raw_header .= '; SameSite=' . $options['samesite'];
								}
							}

							$was_header_sent = true;
							$previous_handler = null;

							// Check whether the cookie was sent.
							$previous_handler = set_error_handler(function() use(&$was_header_sent, &$previous_handler) {
								$was_header_sent = false;
								$result = null;

								if ($previous_handler !== null) {
									// Call custom error handler (if any).
									$result = call_user_func_array($previous_handler, func_get_args());
								} else {
									// Call default error handler.
									$result = false;
								}

								return $result;
							});

							// Send cookie.
							header($raw_header, false);

							restore_error_handler();
							$result = $was_header_sent;
						}
					}
				} else {
					// A legacy setcookie call.

					// PHP 7.4 returns false (instead of true) when any parameter contains NUL.
					if (
						   (isset($args[0]) && is_string($args[0]) && strpbrk($args[0], "\0") !== false)
						|| (isset($args[1]) && is_string($args[1]) && strpbrk($args[1], "\0") !== false)
					) {
						$invalid = trigger_error('Header may not contain NUL bytes', E_USER_WARNING);
					}

					if (!isset($invalid)) {
						// It may return null.
						$result = call_user_func_array('setrawcookie', $args);
					} else {
						$result = false;
					}
				}
			}

			return $result;
		}
	}

	if (!function_exists('setcookie_compat')) {
		/**
		* Drop-in replacement for standard setcookie() allowing
		* older versions of PHP to send cookies using the PHP 7.4's setrawcookie API
		*
		* @param string $name     Cookie's name
		* @param string $value    Cookie's value. It will be rawurlencouded
		* @param mixed  $options  It can be one of these:
		*                         (1) An expiration date (a timestamp)
		*                         (2) An associative array with
		*                         extra information about the cookie
		*                         being sent. Valid keys are: 'expires'
		*                         (a timestamp), 'path' (string),
		*                         'domain' (string), 'secure' (bool),
		*                         'httponly' (bool), and 'samesite'
		*                         (string).
		* @param string $path     Only available if $options is an
		*                         expiration date. Cookie's path
		* @param string $domain   Only available if $options is an
		*                         expiration date. Cookie's domain
		* @param bool   $secure   Only available if $options is an
		*                         expiration date. Whether it should be
		*                         sent over secure connections
		* @param bool   $httponly Only available if $options is an
		*                         expiration date. Whether it should be
		*                         unavailable for clients
		* @param string $samesite Only available if $options is an
		*                         expiration date. Cookie context; it
		*                         can be 'Strict', 'Lax', or 'None'
		*
		* @return true|false|null true if the cookie was sent
		*                         successfully, false if it was not
		*                         sent; it returns null on some error
		*                         conditions
		*/
		function setcookie_compat() {
			$result = null;
			$php_ver = phpversion();
			$args = func_get_args();

			if (version_compare($php_ver, '8.0') >= 0) {
				$result = call_user_func_array('setcookie', $args);
			} else {
				// PHP 7.4 uses rawurlencode, instead of urlencode.
				if (isset($args[1]) && is_string($args[1])) {
					$args[1] = rawurlencode($args[1]);
				}

				try {
					$result = call_user_func_array('setrawcookie_compat', $args);
				} catch(\ArgumentCountError $e) {
					$message = $e->getMessage();

					if (strpos($message, 'setrawcookie') === 0) {
						$message = 'setcookie' . substr($message, 12);
					}

					throw new \ArgumentCountError($message);
				} catch(\ValueError $e) {
					$message = $e->getMessage();

					if (strpos($message, 'setrawcookie') === 0) {
						$message = 'setcookie' . substr($message, 12);
					}

					throw new \ValueError($message);
				} catch(\TypeError $e) {
					$message = $e->getMessage();

					if (strpos($message, 'setrawcookie') === 0) {
						$message = 'setcookie' . substr($message, 12);
					}

					throw new \TypeError($message);
				}
			}

			return $result;
		}
	}
}
