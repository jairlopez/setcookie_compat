# Setcookie_compat

This library is intended to make it possible to use the PHP 7.4 `setcookie` and
`setrawcookie` API in projects that require PHP 5.6 and above.

## Requirements

It requires `PHP >= 5.6`


## Installation

To install, simply require the `setcookie_compat.php` file under `lib`.

You can also install it via Composer by using the
[Packagist archive](https://packagist.org/packages/jairlopez/setcookie-compat).


## Usage

After installation, two functions named `setcookie_compat` and
`setrawcookie_compat` are available, which are drop-in replacements for the
built-in [`setcookie`](https://www.php.net/manual/en/function.setcookie.php) and
[`setrawcookie`](https://www.php.net/manual/en/function.setrawcookie.php) respectively.

Let's say a project that requires PHP 5.6 uses the built-in `setcookie` in this
way:

    setcookie('name', 'value');

The built-in `setcookie` doesn't allow developers to specify the `SameSite=Lax`
cookie attribute; for doing that, developers can use `setcookie_compat`
instead, and take advantage of the function signature available as of PHP 7.3:

    setcookie_compat('name', 'value', array(
        'samesite' => 'Lax'
    ));


## Testing

This library uses PHP's testing mechanism.  Download PHP's source
code and unpack it under `/usr/src/php`, then run `./run-library-tests.sh`


## Contribution

Your feedback and contributions are really welcome! If you find any way of
improving it please let me know. There are namely two ways of contributing:

- Sending *Merge requests* which improve the functionality of this library.
  Additional test units are also welcome
- Raising descriptive *Issues*

I have spent a significant amount of time developing and testing it so that it
may also be used in other projects as well. If you find it useful, please
consider [making a donation](https://www.paypal.me/jair4321), I really
appreciate it.

I'm glad you can save time and effort by leveraging this library.


## Security Vulnerabilities

If you have found a security issue, please contact me directly at
[jair_lopez4321@hotmail.com](mailto:jair_lopez4321@hotmail.com).
