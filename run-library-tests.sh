#!/bin/bash

# MIT License
#
# Copyright (c) 2020-2021 Jair López <jair_lopez4321@hotmail.com> and contributors
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


PHP_SOURCE_CODE="/usr/src/php"

set -e

apply_patches() {
   while [[ $# -gt 0 ]]; do
      local php_version=$1
      local dir_name=

      if [[ -n "$php_version" ]]; then
         dir_name="patches/below-"$(tr -d "." <<< "$php_version")

         if [[ ! -d "$dir_name" ]] || php -r 'exit(version_compare(phpversion(), "'$php_version'") >= 0 ? 0 : 1);'; then
            dir_name=
         fi
      elif [[ -d patches ]]; then
         dir_name=patches
      fi

      if [[ -n "$dir_name" ]]; then
         find "$dir_name" -mindepth 1 -maxdepth 3 \( \( -type d -not \( -name "theirs" -or -name "ours" \) -prune \) -or \( -type f -name "*.patch" -print0 \) \) | while IFS= read -r -d '' file; do
            echo "Applying patch '$file'..."
            patch -fp0 < "$file" || true
            echo ""
         done
      fi

      shift
   done

   if php -r 'exit(version_compare(phpversion(), "7.4") < 0 ? 0 : 1);'; then
      export TEST_PHP_EXECUTABLE=$(which php)

      if [ -d patches/run-tests ] && php -r 'exit(version_compare(phpversion(), "7.1") < 0 ? 0 : 1);'; then
          echo "Processing patches for 'run-tests.php' ..."
          cp "$PHP_SOURCE_CODE/run-tests.php" .

          patch -fp0 < patches/run-tests/run-tests-70.php.patch \
          || true

          mv run-tests.php "$PHP_SOURCE_CODE/run-tests.php"
      fi
   fi
}

run_tests() {
   local result=0

   while [[ $# -gt 0 && $result -eq 0 ]]; do
      local dir_name=$1

      if [[ -n "$dir_name" ]]; then
         php "$PHP_SOURCE_CODE/run-tests.php" -q "tests/$dir_name" || result=$?

         if [[ "$result" -eq 1 ]]; then
            # Display expected and actual output from failed tests
            echo "Showing log files for 'tests/$dir_name' ..."

            find "tests/$dir_name" \
                -iname "*.log" \
                -print -exec cat '{}' ';' -exec echo "" ';'
         fi
      else
         result=1
      fi

      shift
   done

   return $result
}

apply_patches 8.0

run_tests theirs ours
