--TEST--
setrawcookie() error tests
--INI--
date.timezone=UTC
--FILE--
<?php
require __DIR__ . "/../../lib/setcookie_compat.php";

ob_start();

class Foo{
}


setcookie_compat('name', '1<2', time(), 123.45);
setcookie_compat('name', '1<2', array('expires' => new Foo()));
setcookie_compat('name', '1<2', array('expires' => array('one' => 1, 'two' => 2)));
setcookie_compat('name', '1<2', array(
    'expires' => time(),
    'path' => array(
        'one' => 1, 'two' => 2
    )
));
setcookie_compat('name', '1<2', array(
    'expires' => time(),
    'path' => 'path',
    'domain' => array(
        'one' => 1, 'two' => 2
    )
));
setcookie_compat('name', '1<2', array(
    'expires' => time(),
    'path' => 'path',
    'domain' => 'domain',
    'secure' => array(
        'one' => 1, 'two' => 2
    )
));
setcookie_compat('name', '1<2', array(
    'expires' => time(),
    'path' => 'path',
    'domain' => 'domain',
    'secure' => array(
        'one' => 1, 'two' => 2
    )
));
setcookie_compat('name', '1<2', array(
    'expires' => time(),
    'path' => 'path',
    'domain' => 'domain',
    'secure' => new Foo()
));
setcookie_compat('name', '1<2', array(
    'expires' => time(),
    'path' => 'path',
    'domain' => 'domain',
    'secure' => true,
    'httponly' => array(
        'one' => 1, 'two' => 2
    )
));
setcookie_compat('name', '1<2', array(
    'expires' => time(),
    'path' => 'path',
    'domain' => 'domain',
    'secure' => true,
    'httponly' => new Foo()
));
setcookie_compat('name', '1<2', array(
    'expires' => time(),
    'path' => 'path',
    'domain' => 'domain',
    'secure' => true,
    'httponly' => true,
    'samesite' => array(
        'one' => 1, 'two' => 2
    )
));

var_dump(headers_list());
echo "Done\n";
?>
--EXPECTHEADERS--

--EXPECTF--
Warning: Object of class Foo could not be converted to int in %s on line %d

Warning: Array to string conversion in %s on line %d

Warning: Array to string conversion in %s on line %d

Warning: Array to string conversion in %s on line %d
array(12) {
  [0]=>
  string(%d) "X-Powered-By: PHP/%d.%d.%d"
  [1]=>
  string(%d) "Set-Cookie: name=1%3C2; expires=%s; Max-Age=0; path=123.45"
  [2]=>
  string(%d) "Set-Cookie: name=1%3C2; expires=Thu, 01-Jan-1970 00:00:01 GMT; Max-Age=0"
  [3]=>
  string(%d) "Set-Cookie: name=1%3C2; expires=Thu, 01-Jan-1970 00:00:01 GMT; Max-Age=0"
  [4]=>
  string(%d) "Set-Cookie: name=1%3C2; expires=%s; Max-Age=0; path=Array"
  [5]=>
  string(%d) "Set-Cookie: name=1%3C2; expires=%s; Max-Age=0; path=path; domain=Array"
  [6]=>
  string(%d) "Set-Cookie: name=1%3C2; expires=%s; Max-Age=0; path=path; domain=domain; secure"
  [7]=>
  string(%d) "Set-Cookie: name=1%3C2; expires=%s; Max-Age=0; path=path; domain=domain; secure"
  [8]=>
  string(%d) "Set-Cookie: name=1%3C2; expires=%s; Max-Age=0; path=path; domain=domain; secure"
  [9]=>
  string(%d) "Set-Cookie: name=1%3C2; expires=%s; Max-Age=0; path=path; domain=domain; secure; HttpOnly"
  [10]=>
  string(%d) "Set-Cookie: name=1%3C2; expires=%s; Max-Age=0; path=path; domain=domain; secure; HttpOnly"
  [11]=>
  string(%d) "Set-Cookie: name=1%3C2; expires=%s; Max-Age=0; path=path; domain=domain; secure; HttpOnly; SameSite=Array"
}
Done
