--TEST--
Test setcookie_compat(): Specify cookie name and empty value
--INI--
date.timezone=UTC
--FILE--
<?php
require __DIR__ . "/../../lib/setcookie_compat.php";

ob_start();

echo "*** Testing setcookie_compat(): Specify cookie name and empty value ***\n\n";

var_dump(setcookie_compat('foo', ''));

$headers = headers_list();
var_dump($headers);
echo "Done";
ob_end_flush();
?>
--EXPECTHEADERS--

--EXPECTF--
*** Testing setcookie_compat(): Specify cookie name and empty value ***

bool(true)
array(2) {
  [0]=>
  string(%d) "X-Powered-By: PHP/%s"
  [1]=>
  string(%d) "Set-Cookie: foo=deleted; expires=Thu, 01-Jan-1970 00:00:01 GMT; Max-Age=0"
}
Done
