--TEST--
Test setcookie_compat(): Specify cookie name alone
--INI--
date.timezone=UTC
--FILE--
<?php
require __DIR__ . "/../../lib/setcookie_compat.php";

ob_start();

echo "*** Testing setcookie_compat(): Specify cookie name alone ***\n\n";

try {
	var_dump(setcookie_compat());
} catch (\ArgumentCountError $e) {
	echo $e->getMessage() . \PHP_EOL;
}

try {
	var_dump(setcookie_compat('so=me', null));
} catch (\ValueError $e) {
	echo $e->getMessage() . \PHP_EOL;
}

try {
	var_dump(setcookie_compat('some', null, array('fff')));
} catch (\ValueError $e) {
	echo $e->getMessage() . \PHP_EOL;
}

try {
	var_dump(setcookie_compat('some', 'value', array('fff' => 'ggg')));
} catch (\ValueError $e) {
	echo $e->getMessage() . \PHP_EOL;
}

var_dump(setcookie_compat("tw\0o", 'some'));
var_dump(setcookie_compat( "two", 'some', array('samesite' => "L\0x")));

$headers = headers_list();
var_dump($headers);
echo "Done";
ob_end_flush();
?>
--EXPECTHEADERS--

--EXPECTF--
*** Testing setcookie_compat(): Specify cookie name alone ***

setcookie() expects at least 1 argument, 0 given
setcookie(): Argument #1 ($name) cannot contain "=", ",", ";", " ", "\t", "\r", "\n", "\013", or "\014"
setcookie(): option array cannot have numeric keys
setcookie(): option "fff" is invalid

Warning: Header may not contain NUL bytes in %s on line %d
bool(false)

Warning: Header may not contain NUL bytes in %s on line %d
bool(false)
array(1) {
  [0]=>
  string(%d) "X-Powered-By: PHP/%s"
}
Done
