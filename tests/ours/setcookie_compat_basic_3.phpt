--TEST--
setcookie() tests
--INI--
date.timezone=UTC
--FILE--
<?php
require __DIR__ . "/../../lib/setcookie_compat.php";
setcookie_compat('name');
setcookie_compat('name', '');
setcookie_compat('name', 'value');
setcookie_compat('name', 'space value');
setcookie_compat('name', 'value', array('expires' => 0));
setcookie_compat('name', 'value', array('expires' => $tsp = time() + 5));
setcookie_compat('name', 'value', array('expires' => $tsn = time() - 6));
setcookie_compat('name', 'value', array('expires' => $tsc = time()));
setcookie_compat('name', 'value', array('expires' => 0, 'path' => '/path/'));
setcookie_compat('name', 'value', array('expires' => 0, 'path' => '', 'domain' => 'domain.tld'));
setcookie_compat('name', 'value', array('expires' => 0, 'path' => '', 'domain' => '', 'secure' => TRUE));
setcookie_compat('name', 'value', array('expires' => 0, 'path' => '', 'domain' => '', 'secure' => FALSE, 'httponly' => TRUE));

setcookie_compat('name', 'value', array('expires' => $tsp));
setcookie_compat('name', 'value', array('expires' => $tsn, 'path' => '/path/', 'domain' => 'domain.tld', 'secure' => true, 'httponly' => true, 'samesite' => 'Strict'));
setcookie_compat('name', 'value', array('expires' => $tsn, 'path' => '/path/', 'domain' => 'domain.tld', 'secure' => true, 'httponly' => true, 'samesite' => 'Lax'));
setcookie_compat('name', 'value', array('expires' => $tsn, 'path' => '/path/', 'domain' => 'domain.tld', 'secure' => true, 'httponly' => true, 'samesite' => 'None'));

setcookie_compat('name', 'value', array('expires' => 'bus'));
setcookie_compat('name', 'value', array('expires' => acos(8)));
setcookie_compat('name', 'value', array('expires' => log(0)));

$expected = array(
    'Set-Cookie: name=deleted; expires='.date('D, d-M-Y H:i:s', 1).' GMT; Max-Age=0',
    'Set-Cookie: name=deleted; expires='.date('D, d-M-Y H:i:s', 1).' GMT; Max-Age=0',
    'Set-Cookie: name=value',
    'Set-Cookie: name=space%20value',
    'Set-Cookie: name=value',
    'Set-Cookie: name=value; expires='.date('D, d-M-Y H:i:s', $tsp).' GMT; Max-Age=5',
    'Set-Cookie: name=value; expires='.date('D, d-M-Y H:i:s', $tsn).' GMT; Max-Age=0',
    'Set-Cookie: name=value; expires='.date('D, d-M-Y H:i:s', $tsc).' GMT; Max-Age=0',
    'Set-Cookie: name=value; path=/path/',
    'Set-Cookie: name=value; domain=domain.tld',
    'Set-Cookie: name=value; secure',
    'Set-Cookie: name=value; HttpOnly',
    'Set-Cookie: name=value; expires='.date('D, d-M-Y H:i:s', $tsp).' GMT; Max-Age=5',
    'Set-Cookie: name=value; expires='.date('D, d-M-Y H:i:s', $tsn).' GMT; Max-Age=0; path=/path/; domain=domain.tld; secure; HttpOnly; SameSite=Strict',
    'Set-Cookie: name=value; expires='.date('D, d-M-Y H:i:s', $tsn).' GMT; Max-Age=0; path=/path/; domain=domain.tld; secure; HttpOnly; SameSite=Lax',
    'Set-Cookie: name=value; expires='.date('D, d-M-Y H:i:s', $tsn).' GMT; Max-Age=0; path=/path/; domain=domain.tld; secure; HttpOnly; SameSite=None',
    'Set-Cookie: name=value',
    'Set-Cookie: name=value',
    'Set-Cookie: name=value'
);

$headers = headers_list();
if (($i = count($expected)) > count($headers))
{
    echo "Fewer headers are being sent than expected - aborting";
    return;
}

do
{
    if (strncmp(current($headers), 'Set-Cookie:', 11) !== 0)
    {
        continue;
    }

    if (current($headers) === current($expected))
    {
        $i--;
    }
    else
    {
        echo "Header mismatch:\n\tExpected: "
            .current($expected)
            ."\n\tReceived: ".current($headers)."\n";
    }

    next($expected);
}
while (next($headers) !== FALSE);

echo ($i === 0)
    ? 'OK'
    : 'A total of '.$i.' errors found.';
?>
--EXPECTHEADERS--

--EXPECT--
OK
