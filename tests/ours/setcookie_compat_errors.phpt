--TEST--
setrawcookie() error tests
--INI--
date.timezone=UTC
--FILE--
<?php
require __DIR__ . "/../../lib/setcookie_compat.php";

ob_start();

class Foo{
}

try {
   setcookie_compat();
} catch (\ArgumentCountError $e) {
    echo $e->getMessage() . "\n";
}
try {
    setcookie_compat(new Foo());
} catch (\TypeError $e) {
    echo $e->getMessage() . "\n";
}
try {
    setcookie_compat(array('one' => 1, 'two' => 2));
} catch (\TypeError $e) {
    echo $e->getMessage() . "\n";
}
try {
    setcookie_compat('name', new Foo());
} catch (\TypeError $e) {
    echo $e->getMessage() . "\n";
}
try {
    setcookie_compat('name', array('one' => 1, 'two' => 2));
} catch (\TypeError $e) {
    echo $e->getMessage() . "\n";
}
try {
    setcookie_compat('name', '1<2', new Foo());
} catch (\TypeError $e) {
    echo $e->getMessage() . "\n";
}
try {
    setcookie_compat('name', '1<2', 'string');
} catch (\TypeError $e) {
    echo $e->getMessage() . "\n";
}
try {
    setcookie_compat('name', '1<2', time(), new Foo());
} catch (\TypeError $e) {
    echo $e->getMessage() . "\n";
}
try {
    setcookie_compat('name', '1<2', time(), array('one' => 1, 'two' => 2));
} catch (\TypeError $e) {
    echo $e->getMessage() . "\n";
}
try {
    setcookie_compat('name', '1<2', time(), 'path', new Foo());
} catch (\TypeError $e) {
    echo $e->getMessage() . "\n";
}
try {
    setcookie_compat('name', '1<2', time(), 'path', array('one' => 1, 'two' => 2));
} catch (\TypeError $e) {
    echo $e->getMessage() . "\n";
}
try {
    setcookie_compat('name', '1<2', time(), 'path', 'domain', new Foo());
} catch (\TypeError $e) {
    echo $e->getMessage() . "\n";
}
try {
    setcookie_compat('name', '1<2', time(), 'path', 'domain', array('one' => 1, 'two' => 2));
} catch (\TypeError $e) {
    echo $e->getMessage() . "\n";
}
try {
    setcookie_compat('name', '1<2', time(), 'path', 'domain', true, new Foo());
} catch (\TypeError $e) {
    echo $e->getMessage() . "\n";
}
try {
    setcookie_compat('name', '1<2', time(), 'path', 'domain', true, array('one' => 1, 'two' => 2));
} catch (\TypeError $e) {
    echo $e->getMessage() . "\n";
}
try {
    setcookie_compat('name', '1<2', time(), 'path', 'domain', true, true, new Foo());
} catch (\ArgumentCountError $e) {
    echo $e->getMessage() . "\n";
}
try {
    setcookie_compat('name', '1<2', array(
        'expires' => time(),
        'path' => new Foo()
    ));
} catch (\Error $e) {
    echo $e->getMessage() . "\n";
}
try {
   setcookie_compat('name', '1<2', array(
      'expires' => time(),
      'path' => 'path',
      'domain' => new Foo()
   ));
} catch (\Error $e) {
    echo $e->getMessage() . "\n";
}
try {
   setcookie_compat('name', '1<2', array(
      'expires' => time(),
      'path' => 'path',
      'domain' => 'domain',
      'secure' => true,
      'httponly' => true,
      'samesite' => new Foo()
   ));
} catch (\Error $e) {
    echo $e->getMessage() . "\n";
}

try {
   setcookie_compat('name', '1<2', array(
      'expires' => time(),
      'path' => 'path',
      'domain' => 'domain',
      'secure' => true,
      'httponly' => true,
      'one' => 1,
      'two' => 2
   ));
} catch (\ValueError $e) {
    echo $e->getMessage() . "\n";
}

var_dump(headers_list());
echo "Done\n";
?>
--EXPECTHEADERS--

--EXPECTF--
setcookie() expects at least 1 argument, 0 given
setcookie(): Argument #1 ($name) must be of type string, Foo given
setcookie(): Argument #1 ($name) must be of type string, array given
setcookie(): Argument #2 ($value) must be of type string, Foo given
setcookie(): Argument #2 ($value) must be of type string, array given
setcookie(): Argument #3 ($expires_or_options) must be of type array|int, Foo given
setcookie(): Argument #3 ($expires_or_options) must be of type array|int, string given
setcookie(): Argument #4 ($path) must be of type string, Foo given
setcookie(): Argument #4 ($path) must be of type string, array given
setcookie(): Argument #5 ($domain) must be of type string, Foo given
setcookie(): Argument #5 ($domain) must be of type string, array given
setcookie(): Argument #6 ($secure) must be of type bool, Foo given
setcookie(): Argument #6 ($secure) must be of type bool, array given
setcookie(): Argument #7 ($httponly) must be of type bool, Foo given
setcookie(): Argument #7 ($httponly) must be of type bool, array given
setcookie() expects at most 7 arguments, 8 given
Object of class Foo could not be converted to string
Object of class Foo could not be converted to string
Object of class Foo could not be converted to string
setcookie(): option "one" is invalid
array(4) {
  [0]=>
  string(%d) "X-Powered-By: PHP/%d.%d.%d"
  [1]=>
  string(%d) "Set-Cookie: name=1%3C2; expires=%s; Max-Age=0"
  [2]=>
  string(%d) "Set-Cookie: name=1%3C2; expires=%s; Max-Age=0; path=path"
  [3]=>
  string(%d) "Set-Cookie: name=1%3C2; expires=%s; Max-Age=0; path=path; domain=domain; secure; HttpOnly"
}
Done
