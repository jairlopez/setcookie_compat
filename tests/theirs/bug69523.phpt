--TEST--
setcookie() allows empty cookie name
--FILE--
<?php
require __DIR__ . "/../../lib/setcookie_compat.php";
try {
    setcookie_compat('', 'foo');
} catch (\ValueError $e) {
    echo $e->getMessage() . \PHP_EOL;
}
?>
--EXPECT--
setcookie(): Argument #1 ($name) cannot be empty
