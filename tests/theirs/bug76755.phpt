--TEST--
Bug #76755 (setcookie does not accept "double" type for expire time)
--INI--
date.timezone=UTC
--FILE--
<?php
require __DIR__ . "/../../lib/setcookie_compat.php";
var_dump(setcookie_compat('name', 'value', (float)(time() + 1296000)));
?>
--EXPECT--
bool(true)
