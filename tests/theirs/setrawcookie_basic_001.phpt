--TEST--
Test setrawcookie basic functionality
--CREDITS--
PHP TestFEst 2017 - PHPDublin, PHPSP - Joao P V Martins <jp.joao@gmail.com>
--FILE--
<?php
require __DIR__ . "/../../lib/setcookie_compat.php";

var_dump(setrawcookie_compat('cookie_name', rawurlencode('cookie_content')));
?>
--EXPECT--
bool(true)
